package  
{
	import flash.events.Event;
	import manager.Manager;
	/**
	 * ...
	 * @author Alfajor Team
	 */
	public class PowerupManager extends Manager
	{
		static public const MIN_SPAWN_TIME:Number = 10;
		static public const MAX_SPAWN_TIME:Number = 40;
		
		public function PowerupManager() 
		{
			schedule(onCreatePowerupTime, Utils.getRandomNumberBetween(MIN_SPAWN_TIME, MAX_SPAWN_TIME));
		}
		
		
		private function onCreatePowerupTime(e:Event):void 
		{
			schedule(onCreatePowerupTime, Utils.getRandomNumberBetween(MIN_SPAWN_TIME, MAX_SPAWN_TIME));
			mAddManageable(new Powerup(Main.instance.getSpawnPos()));
		}
		
	}

}