package  
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Alfajor Team
	 */
	public class Powerup extends Entity
	{
		private var mc:MovieClip;	
		private var speed:Point;
		
		public function Powerup(pos:Point) 
		{
			mc = getMovieClip("Powerup");
			Main.instance.getUniverseMC().addChild(mc);
			
			mc.x = pos.x;
			mc.y = pos.y;
			
			speed = getVectorTowardEarth();
			speed.normalize(100);
		}
		
		
		override protected function mUpdate(aTime:Number):void 
		{
			super.mUpdate(aTime);
			
			mc.x += speed.x * aTime;
			mc.y += speed.y * aTime;
			
			if (Main.instance.collidesWithPlayer(this)) {
				mUnregister();
				Main.instance.player.grabPowerup();
				SoundMaster.playFX("powerup");
			}
			
			if (collidesWithEarth()) {
				mUnregister();
			}
		}
		
		
		override public function getMC():MovieClip 
		{
			return mc;
		}
		
		
		override public function mDestroy():void 
		{
			Main.instance.getUniverseMC().removeChild(mc);
			super.mDestroy();
		}
		
	}

}