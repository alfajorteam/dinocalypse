package  
{
	import animation.Animation;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class EarthExplosion extends Manager
	{		
		private var anim:Animation;
		
		public function EarthExplosion(pos:Point) 
		{
			var mc:MovieClip = Main.instance.mediaLibrary.getMovieClip("EarthExplosion");
			mc.x = pos.x;
			mc.y = pos.y;
			mc.scaleX = 1.5;
			mc.scaleY = 1.5;
			
			Main.instance.getUniverseMC().addChild(mc);
			
			anim = (Animation)(mAddManageable(new Animation(mc)));
			
			anim.mCreateState("On", false);
			anim.mSetState("On", onAnimEnd);			
			
			anim.mSetSpeedFactor(30);
		}
		
		
		private function onAnimEnd(anim:Animation):void 
		{
			mUnregister();
		}
		
		
		override public function mDestroy():void 
		{			
			super.mDestroy();
			Main.instance.getUniverseMC().removeChild(anim.mGetMC());
		}
		
	}

}