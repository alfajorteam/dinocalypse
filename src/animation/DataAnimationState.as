package animation 
{
	/**	 
	 * @author Santiago Rosas
	 */
	public class DataAnimationState {		
		
		public var pStateName:String
		public var pIsInLoop:Boolean
		public var pSpeedFactor:Number
		
		public function DataAnimationState(aStateName:String, aIsInLoop:Boolean, aSpeedFactor:Number) {
			pStateName = aStateName
			pIsInLoop = aIsInLoop
			pSpeedFactor = aSpeedFactor
		}
		
	}

}