package  
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class Entity extends Manager
	{
		
		public function Entity() 
		{
			
		}
		
		
		public function getPos():Point
		{
			return new Point(getMC().x, getMC().y);
		}
		
		
		public function getMC():MovieClip
		{
			throw new Error("abstract method");
		}
		
		
		protected function getVectorTowardEarth():Point
		{
			return Main.instance.getCenterOfUniverse().subtract(getPos())
		}
		
		
		protected function collidesWithEarth():Boolean
		{
			return Point.distance(getPos(), Main.instance.getCenterOfUniverse()) < Main.instance.getEarthRadius() + 25;
		}		
		
		protected function createEarthExplosion():void 
		{
			Main.instance.getBulletManager().mAddManageable(new EarthExplosion(getPos()));
		}
	
		
		protected function distanceFromEarth():Number
		{
			return Point.distance(getPos(), Main.instance.getCenterOfUniverse());
		}
		
		
		protected function getMovieClip(name:String):MovieClip
		{
			return Main.instance.mediaLibrary.getMovieClip(name);
		}
		
				
		protected function turnWhite():void 
		{
			getMC().transform.colorTransform = Utils.getWhiteColorTransform();
			schedule(onWhiteEnd, getWhiteDuration());
		}
		
		
		private function getWhiteDuration():Number
		{
			return 0.135;
		}
		
				
		protected function onWhiteEnd(e:Event):void 
		{
			getMC().transform.colorTransform = Utils.getNormalColorTransform();
		}
	}

}