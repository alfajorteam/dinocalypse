package  
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class MeteoriteSpawner 
	{
		private var meteorManager:MeteorManager;
		private var timer:Number = 0;
		private var difficultyChangeTimer:Number = 25000;
		private var difficultyLevel:Number = 0;
		private var debugMode:Boolean = false;
		
		private var levelTimer:Timer;
		
		
		public function MeteoriteSpawner() 
		{
			levelTimer = new Timer(difficultyChangeTimer, 0);
			levelTimer.addEventListener(TimerEvent.TIMER, raiseDifficulty, false, 1, false); 
			levelTimer.start();
			meteorManager = new MeteorManager();
			this.difficultyChangeTimer = difficultyChangeTimer;
		}
		
		public function update(aTime:Number):void
		{
			if (meteorManager.mIsPaused()) return;
			meteorManager.mUpdateIfNotPaused(aTime);
		}
		
		// Modify (mainly increase) meteorManager attributes
		private function raiseDifficulty(e:TimerEvent):void
		{
			difficultyLevel += 1;
			if(debugMode) trace("difficulty level: " + difficultyLevel);
			// Change following values in manager:
			// print old
			printCurrentValues();
			// set new
			calculateNewMinTimeBetweenMeteors();
			calculateNewMaxTimeBetweenMeteors();
			calculateMinMeteorVelocity();
			calculateMaxMeteorVelocity();
			// print new
			printNewValues();
			var minTimeBetweenMeteors:Number = 5;
			var maxTimeBetweenMeteors:Number = 5;
			var minMeteorVelocity:Number = 0;
			var maxMeteorVelocity:Number = 150;
			// reschedule manager
		}
		
		private function calculateNewMinTimeBetweenMeteors():void 
		{
			var n:Number = 0
			var min:Number = getMeteorManager().getMinTimeBetweenMeteors();
			if (difficultyLevel <= 9)
			{
				n = (min * 1.3) / Math.log(min);
			} else if (difficultyLevel <= 20) {
				n = (min * 0.9) / Math.log(min);
			} else if (difficultyLevel <= 50) {
				if (min < 2) {
					n = 1;
				}
			}
			if (n < 1) {
				n = 1;
			}
			getMeteorManager().setMinTimeBetweenMeteors(n);
		}
		
		private function calculateNewMaxTimeBetweenMeteors():void 
		{
			var n:Number = 0
			var min:Number = getMeteorManager().getMinTimeBetweenMeteors();
			if (difficultyLevel <= 9)
			{
				n = (min * 1.3) / Math.log(min);
			} else if (difficultyLevel <= 20) {
				n = (min * 0.9) / Math.log(min);
			} else if (difficultyLevel <= 50) {
				if (min < 2) {
					n = 1;
				}
			}
			if (n < 2) {
				n = 1;
			}
			getMeteorManager().setMaxTimeBetweenMeteors(n);
		}
		
		private function calculateMinMeteorVelocity():void 
		{
			// TODO: Implement me!
		}
		
		private function calculateMaxMeteorVelocity():void 
		{
			// TODO: Implement me!
		}
		
		private function printCurrentValues():void 
		{
			if (!debugMode) return;
			trace("====================OLD===================")
			trace("MinTimeBtwMeteors: " + meteorManager.getMinTimeBetweenMeteors());
			trace("MamTimeBtwMeteors: " + meteorManager.getMaxTimeBetweenMeteors());
			trace("MinMeteorVel: " + meteorManager.getMinMeteorVelocity());
			trace("MaxMeteorVel: " + meteorManager.getMaxMeteorVelocity());
		}
		
		
		private function printNewValues():void 
		{
			if (!debugMode) return;
			trace("====================NEW===================")
			trace("MinTimeBtwMeteors: " + meteorManager.getMinTimeBetweenMeteors());
			trace("MinTimeBtwMeteors: " + meteorManager.getMaxTimeBetweenMeteors());
			
		}
		
		public function setDifficultyChangeTimer(value:Number):void 
		{
			difficultyChangeTimer = value;
		}
		
		public function getMeteorManager():MeteorManager 
		{
			return meteorManager;
		}

		public function destroyAll():void 
		{
			meteorManager.mDestroy();
			levelTimer.removeEventListener(TimerEvent.TIMER, raiseDifficulty, false);
			levelTimer.stop();
		}
		
	}

}