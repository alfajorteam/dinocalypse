package 
{	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import manager.Manager;
	
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class Main extends Sprite 
	{
		static public const SCREEN_WIDTH:Number = 800;
		static public const SCREEN_HEIGHT:Number = 800;
		
		static public const ANGULAR_VELOCITY:Number = 140;
		static public const UNIVERSE_Y_OFFSET:Number = 50;
				
		static public var instance:Main;
		
		public var player:Player;		
		public var keyInput:KeyInput;
		public var mediaLibrary:GameMediaLibrary;		
		public var universeMC:MovieClip;
		private var meteoriteSpawner:MeteoriteSpawner;
		private var powerupManager:PowerupManager;
		
		private var lastTime:Number;
		private var bulletManager:Manager;
		private var _rotation:Number = 0;
		private var _lerpRotation:Number = 0;
		private var _lerpRotationMultiplier:Number = 8;
		private var earth:Earth;
		private var isPaused:Boolean = false;
		private var _gameOver:Boolean = false;
		
		public function Main():void 
		{
			instance = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			
			keyInput = new KeyInput();
			lastTime = getTimer();		
			
			SoundMaster.initSounds();			
		}
		
		
		public function onSpaceKeyDown():void 
		{
			
		}
		
		
		public function getCenterOfUniverse():Point
		{
			return new Point(0,0);
		}

		
		public function getEarthMC():MovieClip
		{
			return getUniverseMC().earth;
		}
		
		
		public function getEarthRadius():Number
		{
			return 100;
		}
		
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			mediaLibrary = new GameMediaLibrary(new < String > ["assets", "playerAssets", "gameOverAssets","CartelStartAnim", "introAssets"], onMediaLoadad);
		}
		
		
		private function onMediaLoadad():void 
		{
			universeMC = mediaLibrary.getMovieClip("Universe");			
			addChild(getUniverseMC());
			universeMC.x = SCREEN_WIDTH / 2;
			universeMC.y = SCREEN_HEIGHT / 2 + UNIVERSE_Y_OFFSET;

			stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			earth = new Earth(universeMC.earth, universeMC.damagedEarth3);
			player = new Player();	
			IntroScreen.instance.show();
			Menu.instance.showBackground();	
		}
			
		//menu llama a esto
		public function startLevel():void
		{			
			stage.focus = null;	
			bulletManager = new Manager();
			powerupManager = new PowerupManager();
			meteoriteSpawner = new MeteoriteSpawner();
			
			SoundMaster.playMusic("music");
		}
		
		public function restartLevel():void
		{
			startLevel();
			earth = new Earth(universeMC.earth, universeMC.damagedEarth3);
			player = new Player();	
			isPaused = false;
			setGameOver(false);
		}
		
		private function onEnterFrame(e:Event):void
		{
			var currentTime:Number = getTimer();
			var timeSinceLastFrame:Number = (currentTime - lastTime) / 1000;
			lastTime = currentTime;	
			if (!isPaused) {
				if (!Menu.instance.isOn()) {
					if (keyInput.isLeftKeyDown) {				
						_rotation +=  player.getWalkVelocity() * timeSinceLastFrame;
					}
					else if (keyInput.isRightKeyDown) {
						_rotation -=  player.getWalkVelocity() * timeSinceLastFrame;
					}
					interpolateRotation(timeSinceLastFrame);
				}
				if (bulletManager != null)
				{	bulletManager.mUpdateIfNotPaused(timeSinceLastFrame);
					player.mUpdateIfNotPaused(timeSinceLastFrame);
					powerupManager.mUpdateIfNotPaused(timeSinceLastFrame);
					earth.mUpdateIfNotPaused(timeSinceLastFrame);
					meteoriteSpawner.update(timeSinceLastFrame);
				}	
				Menu.instance.update();	
			}
			IntroScreen.instance.mUpdateIfNotPaused(timeSinceLastFrame);
		}
		
		private function interpolateRotation(timeSinceLastFrame: Number):void {
			if (_rotation >= 360 || _rotation <= -360) {
				_lerpRotation = 0;
				_rotation = 0;
				_lerpRotation = Utils.lerpDegrees(getUniverseMC().rotation, _rotation, timeSinceLastFrame * _lerpRotationMultiplier);	
			} else {
				_lerpRotation = Utils.lerpDegrees(getUniverseMC().rotation, _rotation, timeSinceLastFrame * _lerpRotationMultiplier);	
				
			}
			getUniverseMC().rotation = _lerpRotation;
		}
		
		
		public function getBulletManager():Manager
		{
			return bulletManager;
		}
		
		
		public function getRotation():Number
		{
			return _rotation;
		}
		
		
		public function getUniverseMC():MovieClip
		{
			return universeMC;
		}
		public function meteorHitsEarth():void 
		{
			earth.takeHit();
		}
		
		public function collidesWithPlayer(powerup:Powerup):Boolean
		{
			return player.collidesWith(powerup);
		}
		
		public function getSpawnPos():Point 
		{		
			var xOffset:Number = Main.SCREEN_WIDTH / 2;
			var yOffset:Number = Main.SCREEN_HEIGHT;					
			var offsetVector:Point = new Point(xOffset, yOffset);
			offsetVector = Utils.rotateVector(offsetVector, Utils.getRandomNumberBetween(0, 360));
			
			offsetVector.normalize(600);
			
			return getCenterOfUniverse().add(offsetVector);

		}

		
		public function pause():void 
		{
			isPaused = true;
		}
		
		public function getMeteoriteSpawner():MeteoriteSpawner 
		{
			return meteoriteSpawner;
		}
		
		public function getGamePlayTimer():Number
		{
			return this.player.getSurvivalTime();
		}
		
		public function setGameOver(gameOver:Boolean):void
		{
			this._gameOver = gameOver;
		}
		
		public function isGameOver():Boolean
		{
			return _gameOver;
		}

	}
	
}
