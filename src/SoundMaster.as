package  
{
	import flash.utils.Dictionary;
	import treefortress.sound.SoundAS;
	/**
	 * ...
	 * @author ...
	 */
	public class SoundMaster 
	{
		private static var volumes:Dictionary; //of (String, Number) (Volumes by sound name)
		
		static public function playFX(soundName:String):void
		{			
			SoundAS.play(soundName, volumes[soundName]);
		}
		
		
		static public function playMusic(soundName:String):void
		{			
			SoundAS.playLoop(soundName, volumes[soundName]);
		}		
		
		
		static public function stopSound(soundName:String):void
		{
			SoundAS.pause(soundName);
		}
		
		
		static private function loadSound(soundName:String, volume:Number = 1.0):void
		{
			SoundAS.loadSound("sounds/" + soundName + ".mp3", soundName);
			volumes[soundName] = volume;
		}
		
		
		static public function initSounds():void 
		{	
			volumes = new Dictionary();
			
			loadSound("music", 1);
			loadSound("explosion", 0.5);
			loadSound("shoot", 0.4);
			loadSound("shoot", 0.2);
			loadSound("powerup");
			loadSound("earthExplosion");
			loadSound("flamingMeteorExplosion");
			loadSound("gameover", 1);
		}		
		
		static public function initGameMusic():void
		{
			stopSound("gameover");

			playMusic("music");	
		}
		
		static public function initGameOverMusic():void
		{
			stopSound("music");
			playMusic("gameover");	
		}
		
	}

}