package  
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class GameOverScreen 
	{
		//SINGLETON-------------------------------------------		
		
		static private var _instance:GameOverScreen;
		
		static public function get instance():GameOverScreen
		{
			if (_instance == null) {
				_instance = new GameOverScreen();
			}
			return _instance;
		}
		
		//-----------------------------------------------------
		
		private var curtainMC:MovieClip;
		private var boardsMC:MovieClip;
		private var replayButton:MovieClip;
		
		public function GameOverScreen() 
		{
			curtainMC = Main.instance.mediaLibrary.getMovieClip("Curtain");
			curtainMC.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			boardsMC = Main.instance.mediaLibrary.getMovieClip("Boards");		
		
			boardsMC.timeText.visible = false;
			boardsMC.buttonMode = false;
			boardsMC.timeText.selectable = false;
			boardsMC.timeText.text = getFormattedGamePlayTimerString();
			
			replayButton = Main.instance.mediaLibrary.getMovieClip("ReplayButton");	
		}
		
		private function getFormattedGamePlayTimerString():String
		{
			var gamePlayTimerString:String = Math.round(Main.instance.getGamePlayTimer()).toString();
			
			var s:String = "0000".substr(0, 4 - gamePlayTimerString.length);
			//s = addSpaces(a);
			gamePlayTimerString = s + gamePlayTimerString;
			return gamePlayTimerString;
		}
		
		private function addSpaces(string:String, length:Number):String
		{
			for (var i:int = 0; i < length; i++) {
				// TODO: Add spaces because text doesn't fit well in sprite
				// No time for this...
			}
			return string;
		}
		
		private function onEnterFrame(e:Event):void 
		{
			Main.instance.setGameOver(true);
			if (curtainMC.currentLabel == "End") {
				curtainMC.stop();
				
				boardsMC.gameOver.stop();
				boardsMC.time.stop();
				boardsMC.empty.stop();
				boardsMC.timeText.visible = true;
				replayButton.stop();
				replayButton.addEventListener(MouseEvent.CLICK, onReplayButtonClick);
			}
		}
		
		
		private function onReplayButtonClick(e:MouseEvent):void 
		{
			boardsMC.visible = false;
			replayButton.visible = false;
			curtainMC.visible = false;
			
			Main.instance.getMeteoriteSpawner().destroyAll();
			Main.instance.restartLevel();
		}
		
		public function show():void
		{
			Main.instance.addChild(curtainMC);
			curtainMC.x = 402;
			curtainMC.y = 408;
			curtainMC.visible = true;
			
			Main.instance.addChild(boardsMC);
			boardsMC.x = 141;
			boardsMC.y = -537;	
			boardsMC.visible = true;
			
			Main.instance.addChild(replayButton);
			replayButton.x = 400;
			replayButton.y = 700;
			replayButton.visible = true;
		}
		
		// Delete (?)
		public function onSpaceKeyDown():void
		{

		}
		
	}
	
	

}