package  
{
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class Utils 
	{
		
		/**		 
		 * @param	aVector Vector to rotate
		 * @param	aAngle Rotation angle in degrees
		 * 
		 * @return
		 */
		static public function rotateVector(aVector:Point, aAngle:Number):Point 
		{
			if (aAngle == 0) {
				return aVector.clone();
			} else {
				var vAngle:Number = degreesToRadians(aAngle);
				var vX:Number = aVector.x * Math.cos(vAngle) + aVector.y * Math.sin(vAngle);
				var vY:Number = aVector.y * Math.cos(vAngle) - aVector.x * Math.sin(vAngle);
				return new Point(vX, vY);
			}
		}
		
		
		static public function degreesToRadians(aAngleDegrees:Number):Number {
			return aAngleDegrees / 360 * (2 * Math.PI);
		}
	
		
		static public function getRandomNumberBetween(aNumber1:Number, aNumber2:Number):Number {
			return aNumber1 + (Math.random() * (aNumber2 - aNumber1))
		}
	
		
		static public function getOppositeVector(aVector:Point):Point {
			return new Point(0, 0).subtract(aVector)
		}
		
		static public function lerpDegrees(start:Number, end:Number, amount:Number):Number {
			var difference:Number = Math.abs(end - start);
			if (difference > 180)
			{
				// We need to add on to one of the values.
				if (end > start)
				{
					// We'll add it on to start...
					start += 360;
				}
				else
				{
					// Add it on to end.
					end += 360;
				}
			}
	
			// Interpolate it.
			var value:Number = (start + ((end - start) * amount));
	
			// Wrap it..
			var rangeZero:Number = 360;
	
			if (value >= 0 && value <= 360)
				return value;
	
			return (value % rangeZero);
		}
		
		static public function lerp(start:Number, end:Number, percent:Number):Number {
			return (start + percent * (end - start));
		}
		
		static public function nLerp(start:Number, end:Number, percent:Number):Number {
			return lerp(start, end, percent);
		}
		
		static public function approach(goal:Number, current:Number, delta:Number):Number {
			var diff:Number = goal - current;
			if (diff > delta) {
				return current + delta;
			}
			if (diff < -delta) {
				return current - delta;
			}
			return goal;
		}
		
		/**
		 * @return The angle between aVector1 and aVector2 in degrees.
		 * If aVector2 is null or not specified, the vector (0,1) (Y axis) is assumed.
		 */
		static public function getAngleBetweenVectors(aVector1:Point, aVector2:Point = null):int 
		{
			var vVector1:Point = aVector1.clone()
			vVector1.normalize(1.0)
			var vVector2:Point = aVector2 == null ? new Point(0,1) : aVector2.clone()			
			vVector2.normalize(1.0)
			
			var vAngle:int = Math.acos(Utils.dotProduct(vVector1, vVector2)) * 360 / (2 * Math.PI);
			
			if (vVector1.x > 0.0) {
				vAngle = -vAngle;
			}
			return vAngle
		}
		
		
		static public function dotProduct(aPoint1:Point, aPoint2:Point):Number {
			return (aPoint1.x * aPoint2.x + aPoint1.y * aPoint2.y)
		}
		
		
		static public function getWhiteColorTransform():ColorTransform
		{
			 return new ColorTransform(1, 1, 1, 1, 1000, 1000, 1000);
		}
		
		
		static public function getNormalColorTransform():ColorTransform
		{			 
			 return new ColorTransform(1, 1, 1, 1, 0, 0, 0);
		}
	}

}