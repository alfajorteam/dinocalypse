package  
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class MeteorManager extends Manager
	{
		private var _minTimeBetweenMeteors:Number = 5;
		private var _maxTimeBetweenMeteors:Number = 8;
		
		private var _minMeteorVelocity:Number = 0;
		private var _maxMeteorVelocity:Number = 50;
		private var _flamingMeteoriteVelocity:Number = 80;
		
		public function MeteorManager()		
		{
			onSpawnMeteorTime(null);
			schedule(onSpawnMeteorTime, getRandomTimeBetweenMeteors());
		}
		
		
		private function getRandomTimeBetweenMeteors():Number
		{
			return Utils.getRandomNumberBetween(_minTimeBetweenMeteors, _maxTimeBetweenMeteors);
		}
		
		
		public function getCollidingMeteor(entity:Entity):Meteor
		{
			for each (var meteor:Meteor in mGetManageables()) {
				if (meteor != entity && meteor.collidesWith(entity)) {
					return meteor;
				}
			}
			return null;
		}
		
		
		private function onSpawnMeteorTime(e:Event):void 
		{
			var meteor:Meteor;
			if (Math.random() < 0.2) {
				meteor = new FlamingMeteor(Main.instance.getSpawnPos(), _flamingMeteoriteVelocity);
			}
			else {
				meteor = new Meteor(Main.instance.getSpawnPos(), Utils.getRandomNumberBetween(_minMeteorVelocity, _maxMeteorVelocity));		
			}
			mAddManageable(meteor);
			
			schedule(onSpawnMeteorTime, getRandomTimeBetweenMeteors());
		}
		
		
		private function getSpawnPos():Point 
		{		
			var xOffset:Number = Main.SCREEN_WIDTH / 2;
			var yOffset:Number = Main.SCREEN_HEIGHT;					
			var offsetVector:Point = new Point(xOffset, yOffset);
			offsetVector = Utils.rotateVector(offsetVector, Utils.getRandomNumberBetween(0, 360));
			
			offsetVector.normalize(600);
			
			return Main.instance.getCenterOfUniverse().add(offsetVector);
		}
		
		public function getMinTimeBetweenMeteors():Number 
		{
			return _minTimeBetweenMeteors;
		}
		
		public function setMinTimeBetweenMeteors(value:Number):void 
		{
			_minTimeBetweenMeteors = value;
		}
		
		public function getMaxTimeBetweenMeteors():Number 
		{
			return _maxTimeBetweenMeteors;
		}
		
		public function setMaxTimeBetweenMeteors(value:Number):void 
		{
			_maxTimeBetweenMeteors = value;
		}
		
		public function getMinMeteorVelocity():Number 
		{
			return _minMeteorVelocity;
		}
		
		public function setMinMeteorVelocity(value:Number):void 
		{
			_minMeteorVelocity = value;
		}
		
		public function getMaxMeteorVelocity():Number 
		{
			return _maxMeteorVelocity;
		}
		
		public function setMaxMeteorVelocity(value:Number):void 
		{
			_maxMeteorVelocity = value;
		}
		
	}

}