package  
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import manager.Manager;
	/**
	 * ...
	 * @author AlfajorTeam
	 */
	public class Menu 
	
	{
		//SINGLETON-------------------------------------------
		static private var _instance:Menu
		static public function get instance():Menu
		{
		if (_instance == null) {
			_instance = new Menu();
			}
			return _instance;
		}
		//-----------------------------------------------------
		private var Telon:MovieClip;
		private var CartelStart:MovieClip;
		private var Btn_Play:flash.display.SimpleButton;
		private var Black_Background:MovieClip;
		
		public function Menu() 
		{
			Telon = Main.instance.mediaLibrary.getMovieClip("Telon");
			Main.instance.addChild(Telon);
			Telon.x = Main.SCREEN_WIDTH / 2;
			Telon.y = Main.SCREEN_HEIGHT / 2;
			Telon.gotoAndStop(0);
			CartelStart = Main.instance.mediaLibrary.getMovieClip("CartelStart");
			Main.instance.addChild(CartelStart);
			CartelStart.x = Main.SCREEN_WIDTH / 2;
			CartelStart.y = Main.SCREEN_HEIGHT / 4;
			CartelStart.height = CartelStart.height * 5;
			CartelStart.width = CartelStart.width * 5;
			CartelStart.gotoAndStop(0);
			Black_Background = Main.instance.mediaLibrary.getMovieClip("Black_Background");
			Main.instance.addChild(Black_Background);
			Black_Background.x = -600;
			Black_Background.y = -100;
			Btn_Play = Main.instance.mediaLibrary.getSimpleButton("Btn_Play");
			Main.instance.addChild(Btn_Play).addEventListener(MouseEvent.CLICK, onClickPlay, false, 0, true);
			Btn_Play.height = Btn_Play.height * 1.3;
			Btn_Play.width = Btn_Play.width * 1.5;
			Btn_Play.x = Main.SCREEN_WIDTH / 2;
			Btn_Play.y = Main.SCREEN_HEIGHT - 300;
			
		}		
		
		public function showBackground():void
		{
			Telon.visible = true
			CartelStart.visible = true
		}
		public function hideBackground():void
		{
			Telon.visible = false
			CartelStart.visible = false
		}
		
		
		public function isOn():Boolean
		{
			return Telon.visible;
		}
		
		
		public function update():void
		{
			if (Telon.currentFrameLabel == "Fin_Telon")
			{
				Telon.stop();
				CartelStart.stop()
				hideBackground();
				Telon.gotoAndStop(0)
				CartelStart.gotoAndStop(0)				
				IntroScreen.instance.startAnimation();	
			}
		}
		public function PlayAnimationAndDestroy():void
		{
			Telon.gotoAndPlay(0);
			CartelStart.gotoAndPlay(0);
			
		}
		public function onClickPlay(aEvent:MouseEvent):void
		{
			Btn_Play.visible = false;
			PlayAnimationAndDestroy();
		}
	}
}