package  
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class IntroScreen extends Manager
	{
		static private const READ_TIME:Number = 14;
		
		static public const STATE_APPEARING:String = "STATE_APPEARING";		
		static public const STATE_READING:String = "STATE_READING";		
		static public const STATE_OFF:String = "STATE_OFF";
		
		//SINGLETON-------------------------------------------		
		
		static private var _instance:IntroScreen;
		
		static public function get instance():IntroScreen
		{
			if (_instance == null) {
				_instance = new IntroScreen();
			}
			return _instance;
		}
		
		//-----------------------------------------------------
		
		private var mc:MovieClip;
		private var blendSpeed:Number;
		private var state:String;
		private var hasBeenShown:Boolean = false;
		
		public function IntroScreen() 
		{
			mc = Main.instance.mediaLibrary.getMovieClip("IntroScreen");
			Main.instance.addChild(mc);		
			mc.addEventListener(Event.ENTER_FRAME, onEnterFrame);			
			mc.x = 58;
			trace("new intro screen");
		}
		
		
		public function show():void
		{
			mc.y = 347;
			mc.stop();
			mc.title.stop();
			blendSpeed = 0;
			mc.alpha = 1;
			state = STATE_APPEARING;
			
			trace("SHOW");
		}
		
		
		private function onEnterFrame(e:Event):void 
		{
			if (mc.currentLabel == "Read" && state == STATE_APPEARING) {
				trace("mc stop");
				mc.stop();			
				schedule(onReadTimeUp, READ_TIME);
				state = STATE_READING;
			}			
			
			if (hasBeenShown) {
				mc.visible = false;
			}
		}
		
		
		private function onReadTimeUp(e:Event):void 
		{
			trace("on read time up");
			blendSpeed = -1;
		}		
		
		
		public function startAnimation():void
		{
			("start animation");
			mc.play();		
			mc.title.play();
		}		
		
		
		override protected function mUpdate(aTime:Number):void 
		{
			super.mUpdate(aTime);
			
			mc.alpha += blendSpeed * aTime;
						
			if (mc.alpha <= 0 && state == STATE_READING) {
				Main.instance.startLevel();
				state = STATE_OFF;
				trace("OFF"); 
				hasBeenShown = true;
			}		
			
			if (hasBeenShown) {
				mc.visible = false;
			}
		}
		
		
		public function isOn():Boolean
		{
			return state == STATE_APPEARING || state == STATE_READING;

		}

	}

}