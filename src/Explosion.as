package  
{
	import animation.Animation;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class Explosion extends Manager
	{		
		private var anim:Animation;
		
		public function Explosion(pos:Point) 
		{
			var mc:MovieClip = Main.instance.mediaLibrary.getMovieClip("BulletExplosion");
			mc.x = pos.x;
			mc.y = pos.y;
			
			mc.scaleX = 0.3;
			mc.scaleY = 0.3;
			
			Main.instance.getUniverseMC().addChild(mc);
			
			anim = (Animation)(mAddManageable(new Animation(mc)));
			
			anim.mCreateState("On", false);
			anim.mSetState("On", onAnimEnd);			
			
			anim.mSetSpeedFactor(30);
		}
		
		
		private function onAnimEnd(anim:Animation):void 
		{
			mUnregister();
		}
		
		
		override public function mDestroy():void 
		{			
			super.mDestroy();
			Main.instance.getUniverseMC().removeChild(anim.mGetMC());
			anim.mGetMC().visible = false;
		}
		
	}

}