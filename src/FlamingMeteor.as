package  
{
	import animation.Animation;
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Alfajor Team
	 */
	public class FlamingMeteor extends Meteor
	{
		
		private var flamesAnim:Animation;
		
		public function FlamingMeteor(pos:Point, velocity:Number) 
		{
			super(pos, velocity);
			
			var flamesMC:MovieClip = Main.instance.mediaLibrary.getMovieClip("MeteorFlames");
			flamesMC.x = getPos().x;
			flamesMC.y = getPos().y;
						
			Main.instance.getUniverseMC().addChild(flamesMC);
			Main.instance.getUniverseMC().addChild(getMC());
			
			flamesAnim = (Animation)(mAddComponent(new Animation(flamesMC)));
			flamesAnim.mCreateState("On", true);
			flamesAnim.mSetState("On");
			
			flamesMC.rotation = Utils.getAngleBetweenVectors(new Point(0, 1), getVectorTowardEarth());

			var vector:Point = Utils.getOppositeVector(getVectorTowardEarth());
			
			if (vector.x > 0) {
				var angle:Number = Utils.getAngleBetweenVectors(new Point(0, -1), vector);					
				flamesMC.rotation = angle;
			}
			else {
				angle = Utils.getAngleBetweenVectors(new Point(0, 1), vector);
				flamesMC.rotation = 180;
				flamesMC.rotation += angle;
			}				

		}
		
		
		override protected function mUpdate(aTime:Number):void 
		{
			super.mUpdate(aTime);
			flamesAnim.mGetMC().x = getPos().x;
			flamesAnim.mGetMC().y = getPos().y;
		}

		
		override public function mDestroy():void 
		{
			Main.instance.getUniverseMC().removeChild(flamesAnim.mGetMC());
			super.mDestroy();
		}
		
		
		override public function takeHit(bulletPos:Point):void 
		{
			createEarthExplosion();
			mUnregister();
			SoundMaster.playFX("explosion");
			SoundMaster.playFX("flamingMeteorExplosion");
		}
	}

}