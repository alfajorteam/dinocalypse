package  
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	import manager.Manageable;
	import manager.Manager;
	/**
	 * ...
	 * @author Team Alfajor
	 */
	public class Bullet extends Entity
	{
		
		private var mc:MovieClip;
		private var speed:Point;
		private var acceleration:Point;
		
		public function Bullet(pos:Point, velocity:Number) 
		{
			mc = Main.instance.mediaLibrary.getMovieClip("Bullet");
			Main.instance.getUniverseMC().addChild(mc);			
			
			mc.x = pos.x;
			mc.y = pos.y;
			
			speed = getPos().subtract(Main.instance.getCenterOfUniverse());			
			speed.normalize(velocity);
			
			//acceleration = getVectorTowardEarth();
			//acceleration.normalize(2000);
			
			acceleration = new Point(0, 0);
		}
		
		
		override protected function mUpdate(aTime:Number):void 
		{			
			super.mUpdate(aTime);
			mc.x += speed.x * aTime;
			mc.y += speed.y * aTime;
			
			speed.x += acceleration.x * aTime;
			speed.y += acceleration.y * aTime;
			
			var collidedMeteor:Meteor = Main.instance.getMeteoriteSpawner().getMeteorManager().getCollidingMeteor(this);
			if (collidedMeteor != null) {
				mUnregister();
				collidedMeteor.takeHit(getPos());
				createExplosion();
			}
			
			if (collidesWithEarth()) {
				mUnregister();
				SoundMaster.playFX("explosion");
			}
			if (distanceFromEarth() > 1000) {
				mUnregister();				
			}
		}
		
		
		private function createExplosion():void 
		{
			Main.instance.getBulletManager().mAddManageable(new Explosion(getPos()));
		}
		
		
		override public function mDestroy():void 
		{
			Main.instance.getUniverseMC().removeChild(mc);
			super.mDestroy();
		}
		
		
		override public function getMC():MovieClip 
		{
			return mc;
		}
		
		
		
		
	}

}