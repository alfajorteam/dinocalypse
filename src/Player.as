package  
{
	import animation.Animation;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.globalization.NumberParseResult;
	import flash.utils.Timer;
	import manager.Manager;
	/**
	 * ...
	 * @author Alfajor Team
	 */
	public class Player extends Entity
	{
		static private const Y:Number = 300 + Main.UNIVERSE_Y_OFFSET;
		static private const POWERUP_DURATION:Number = 15;
		static private const NORMAL_SHOT_COOLDOWN:Number = 0.5;
		static private const POWERUP_SHOT_COOLDOWN:Number = 0.25;
		static private const NORMAL_WALK_VELOCITY:Number = 140;
		static private const POWERUP_WALK_VELOCITY:Number = 240;
		
		
		private var canShoot:Boolean = true;
		private var lowerAnim:Animation;
		private var upperAnim:Animation;
		private var isPoweredUp:Boolean;
		
		private var survivalTimer:Timer;
		private var survivalSeconds:Number = 0;
		
		
		public function Player()
		{
			survivalTimer = new Timer(1000, 0);
			survivalTimer.addEventListener(TimerEvent.TIMER, increaseSurvivalSeconds, false, 1, false);
			survivalTimer.start();
			var offset:Point = new Point( -20, -55);		
			var lowerMC:MovieClip = Main.instance.mediaLibrary.getMovieClip("LowerPlayer");			
			Main.instance.addChild(lowerMC);			
			lowerMC.x = Main.SCREEN_WIDTH / 2 + offset.x;
			lowerMC.y = Y + offset.y;				
			
			lowerAnim = (Animation)(mAddComponent(new Animation(lowerMC)));
			lowerAnim.mCreateState("Idle", true);
			lowerAnim.mCreateState("Walk", true);
			lowerAnim.mSetState("Idle");	
			
			var upperMC:MovieClip = Main.instance.mediaLibrary.getMovieClip("UpperPlayer");			
			Main.instance.addChild(upperMC);
			upperMC.x = Main.SCREEN_WIDTH / 2 + offset.x;
			upperMC.y = Y + offset.y;					
			
			upperAnim = (Animation)(mAddComponent(new Animation(upperMC)));
			upperAnim.mCreateState("Idle", true);
			upperAnim.mCreateState("Shoot", false);
			upperAnim.mSetState("Idle");			

			isPoweredUp = false;
		}
		
		private function increaseSurvivalSeconds(e:TimerEvent):void 
		{
			survivalSeconds += 1;
		}
		
		
		public function onSpaceKeyDown():void 
		{
			shootIfAble();
			
		}
		
		
		private function onCanShootTime(e:Event):void
		{
			canShoot = true;
		}
		
		
		private function getBulletPos():Point
		{
			var x:Number;
			if (upperAnim.mGetMC().scaleX == 1) {
				x = 43;
			}
			else {
				x = -44;
			}
			x = 0;
			
			var offset:Point = new Point(x, -78);
			var posVector:Point = new Point(0, -110);			
			posVector = posVector.add(offset);
			
			posVector = Utils.rotateVector(posVector.clone(), Main.instance.getRotation());			
			
			return posVector;
		}

		
		private function shootIfAble():void 
		{
			if (canShoot && !Menu.instance.isOn() && !IntroScreen.instance.isOn()) {
				var bullet:Bullet = new Bullet(getBulletPos(), 1000);
				Main.instance.getBulletManager().mAddManageable(bullet);			
				canShoot = false;
				schedule(onCanShootTime, getShotCooldown());
				SoundMaster.playFX("shoot");
				upperAnim.mSetState("Shoot");
			}
		}
		
		
		private function getShotCooldown():Number
		{
			if (isPoweredUp) {
				return POWERUP_SHOT_COOLDOWN;
			}
			else {
				return NORMAL_SHOT_COOLDOWN;
			}
		}
		
		
		override protected function mUpdate(aTime:Number):void
		{
			super.mUpdate(aTime);
			if (Main.instance.keyInput.isSpaceKeyDown) {
				shootIfAble();
			}
			
			if (Main.instance.keyInput.isLeftKeyDown && Main.instance.keyInput.isRightKeyDown) {
				return;
			}
			
			if (lowerAnim.mGetMC().scaleX == 1 && Main.instance.keyInput.isLeftKeyDown)  {
				lowerAnim.mGetMC().scaleX = -1;
				upperAnim.mGetMC().scaleX = -1;
				lowerAnim.mGetMC().x += 50;
				upperAnim.mGetMC().x += 50;
			}
			else if (lowerAnim.mGetMC().scaleX == -1 && Main.instance.keyInput.isRightKeyDown)  {
				lowerAnim.mGetMC().scaleX = 1;
				upperAnim.mGetMC().scaleX = 1;	
				lowerAnim.mGetMC().x -= 50;
				upperAnim.mGetMC().x -= 50;
			}
			
			if (Main.instance.keyInput.isLeftKeyDown || Main.instance.keyInput.isRightKeyDown) {
				if (lowerAnim.mGetState() == "Idle") {
					lowerAnim.mSetState("Walk");
				}
			}
			else {
				if (lowerAnim.mGetState() == "Walk") {
					lowerAnim.mSetState("Idle");
				}
			}
		}
		
		
		override public function getMC():MovieClip 
		{
			return upperAnim.mGetMC();
		}

		
		public function collidesWith(powerup:Powerup):Boolean
		{
			//return Point.distance(getPos(), powerup.getPos()) < 60;
			
			return upperAnim.mGetMC().hitTestObject(powerup.getMC());
		}
		
		
		public function grabPowerup():void 
		{
			isPoweredUp = true;
			schedule(onPowerupTimeUp, POWERUP_DURATION);
		}
		
		
		public function getWalkVelocity():Number
		{
			if (isPoweredUp) {
				return POWERUP_WALK_VELOCITY;
			}
			else {
				return NORMAL_WALK_VELOCITY;
			}
		}
		
		public function getSurvivalTime():Number
		{
			return this.survivalSeconds;
		}
		
		
		private function onPowerupTimeUp(e:Event):void 
		{
			isPoweredUp = false;
		}
		
		override public function mDestroy():void 
		{
			Main.instance.removeChild(upperAnim.mGetMC());
			Main.instance.removeChild(lowerAnim.mGetMC());
			survivalTimer.removeEventListener(TimerEvent.TIMER, increaseSurvivalSeconds, false);
			survivalTimer.stop();
			
			super.mDestroy();
		}
	}
 
}