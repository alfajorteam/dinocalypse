package
{
	import animation.Animation;
	import flash.display.MovieClip;
	import flash.events.Event;
	/**
	 * ...
	 * @author Alfajor Team
	 */
	public class Earth extends Entity
	{
		private var mc:MovieClip;
		private var currentStateHP:int;
		private var explosionAnim:Animation;
		private var damaged3MC:MovieClip;

		
		public function Earth(aMC:MovieClip, damaged3MC:MovieClip) 
		{
			mc = aMC;
			mc.gotoAndStop("Normal");
			mc.visible = true;

			currentStateHP = 2;
			
			onWhiteEnd(null);
			damaged3MC.visible = false;
			this.damaged3MC = damaged3MC;
		}

		
		public function takeHit():void 
		{
			currentStateHP--;
			
			turnWhite();
			
			if (currentStateHP == 0) {


				currentStateHP = 2;
				if (mc.currentLabel == "Normal") {
					mc.gotoAndStop("Damaged1");
				}
				else if (mc.currentLabel == "Damaged1") {
					mc.gotoAndStop("Damaged2");
				}
				else if (mc.currentLabel == "Damaged2") {
					mc.gotoAndStop("Damaged3");
					mc.visible = false;
					damaged3MC.visible = true;
				}
				else if (mc.currentLabel == "Damaged3") {
					explode();

				}				
			}
			
		}
		
		
		private function onExplosionAnimEnd(anim:Animation):void 
		{
			explosionAnim.mPause();
			explosionAnim.mGetMC().visible = false;
			schedule(onGameOverTime, 1);			
			Main.instance.universeMC.removeChild(explosionAnim.mGetMC());			
			mc.visible = false;
			Main.instance.player.mDestroy();
			damaged3MC.visible = false;
		}
		
		
		private function onGameOverTime(e:Event):void
		{

			GameOverScreen.instance.show();
			Main.instance.pause();
		}
		
		private function explode():void 
		{
			var explosionMC:MovieClip = getMovieClip("EarthFinalExplosion");
			Main.instance.universeMC.addChild(explosionMC);
			explosionMC.x = 0;
			explosionMC.y = 0;
			explosionAnim = (Animation)(mAddComponent(new Animation(explosionMC)));
			explosionAnim.mCreateState("On", false);
			explosionAnim.mSetState("On", onExplosionAnimEnd);
			explosionAnim.mGetMC().visible = true;
			
			SoundMaster.playFX("earthExplosion");



		}
		
		
		override public function getMC():MovieClip 
		{
			return mc;
		}
		
		override protected function turnWhite():void 
		{
			damaged3MC.transform.colorTransform = Utils.getWhiteColorTransform();
			super.turnWhite();
		}
		
		
		override protected function onWhiteEnd(e:Event):void 
		{
			if (damaged3MC != null) {
				damaged3MC.transform.colorTransform = Utils.getNormalColorTransform();
			}
			super.onWhiteEnd(e);
		}
	}

}