### What is this repository for? ###
Game project for the [Global Game Jam 2015](http://globalgamejam.org/2015/games/dinocalypse) by the [Alfajor Team](https://bitbucket.org/alfajorteam) (yeah, deal with it)

The **Dinocalypse**

* Quick summary
>Play the role of a wise dinosaur in possession of an ancient magical staff with the power to save his planet from infinite asteroid attacks. Extinction might be inevitable, but a true hero doesn't give up.

### Contribution guidelines ###
>Anyone from the team can do whatever they want, also anybody can use use the content of this repository under the license specified below

### LICENSE ###
>This game and all content in this repository is licensed under the Attribution-Noncommercial-Share Alike 3.0 version of the Creative Commons License.
>For reference the license is given below and can also be found at http://creativecommons.org/licenses/by-nc-sa/3.0/

### Playable game ###
>We uploaded a playable version to dropbox: [dinocalypse](https://dl.dropboxusercontent.com/u/7828842/Dinocalypse2/index.html)
>
>You need to have flash installed in your browser, It's a flash game...

### Who do I talk to? ###

* Repo owner or admins